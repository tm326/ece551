#include <cmath>
#include <cstdio>
#include <cstdlib>

#include "function.h"

int binarySearchForZero(Function<int, int> * f, int low, int high) {
  int left = low;
  int right = high;
  int mid;
  int ans;
  while (left < right) {
    mid = floor((left + right) / 2.0);
    ans = f->invoke(mid);
    //printf("left:%d, right:%d, mid:%d, ans:%d\n", left, right, mid, ans);
    if (ans == 0) {
      return mid;
    }
    else if (ans < 0) {
      left = mid + 1;
    }
    else {
      right = mid;
    }
  }
  return left - 1 < low ? left : left - 1;
}
