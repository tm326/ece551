#include "cassert"
#include "cmath"
#include "cstdio"
#include "cstdlib"
#include "function.h"

int binarySearchForZero(Function<int, int> * f, int low, int high);

class LinearFunction : public Function<int, int> {
 private:
  float offset;
  float a;

 public:
  LinearFunction() : offset(0), a(1){};
  LinearFunction(float offset_in, float a_in) : offset(offset_in), a(a_in){};
  virtual int invoke(int arg) { return a * arg + offset; }
};

class CountedIntFn : public Function<int, int> {
 protected:
  unsigned remaining;
  Function<int, int> * f;
  const char * mesg;

 public:
  CountedIntFn(unsigned n, Function<int, int> * fn, const char * m) :
      remaining(n),
      f(fn),
      mesg(m) {
    //fprintf(stdout, "remaining:%d\n", n);
  }
  virtual int invoke(int arg) {
    //fprintf(stdout, "invoke:arg:%d\n", arg);
    if (remaining == 0) {
      fprintf(stderr, "Too many function invocations in %s\n", mesg);
      exit(EXIT_FAILURE);
    }
    remaining--;
    return f->invoke(arg);
  }
};

void check(Function<int, int> * f,
           int low,
           int high,
           int expected_ans,
           const char * mesg) {
  int maxInvocation = log2(high - low) + 1;
  if (high <= low) {
    maxInvocation = 1;
  }
  CountedIntFn counterFn(maxInvocation, f, mesg);
  int ans = 0;
  ans = binarySearchForZero(&counterFn, low, high);
  if (expected_ans != ans) {
    fprintf(stderr, "unexpected answer %d, %s\n", ans, mesg);
    exit(EXIT_FAILURE);
  }
  return;
}

int main() {
  LinearFunction lfunc;
  check(&lfunc, 3, 10, 3, "test1");
  check(&lfunc, -4, 4, 0, "test2");
  check(&lfunc, -10, -1, -2, "test3");
  check(&lfunc, 0, 5, 0, "test4");
  check(&lfunc, -5, 0, -1, "test5");
  check(&lfunc, 5, -5, 5, "test6");
  check(&lfunc, 0, 0, 0, "test7");
  check(&lfunc, -1, -1, -1, "test8");
  LinearFunction lfunc2(-100, 100);
  check(&lfunc2, -100, 100, 1, "test9");
  LinearFunction lfunc3(-99, 100);
  check(&lfunc3, -100, 100, 0, "test10");
}
