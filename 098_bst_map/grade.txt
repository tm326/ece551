Grading at 11/11/2021:00:30:57.722769
For commit ID 907061b26f0afb676107069c4d8f71c9fc302a18
Grading at Wed Nov 10 19:30:47 EST 2021
-----------------------------------------------
testcase 1 : Testing with BstMap<int,int>
...compiling...
The code compiled.
  - Valgrind was clean (no errors, no memory leaks)
Your file matched the expected output
Testcase passed 
-----------------------------------------------
testcase 2 : Testing with BstMap<int,std::string>
...compiling...
The code compiled.
  - Valgrind was clean (no errors, no memory leaks)
Your file matched the expected output
Testcase passed 
-----------------------------------------------
testcase 3 : Testing with BstMap<int,(a custom class we wrote)>
...compiling...
The code compiled.
  - Valgrind was clean (no errors, no memory leaks)
Your file matched the expected output
Testcase passed 
-----------------------------------------------
testcase 4 : Testing with BstMap<std::string,int>
...compiling...
The code compiled.
  - Valgrind was clean (no errors, no memory leaks)
Your file matched the expected output
Testcase passed 
-----------------------------------------------
testcase 5 : Testing with BstMap<std::string,std::string>
...compiling...
The code compiled.
  - Valgrind was clean (no errors, no memory leaks)
Your file matched the expected output
Testcase passed 
-----------------------------------------------
testcase 6 : Testing with BstMap<std::string,(a custom class we wrote)>
...compiling...
The code compiled.
  - Valgrind was clean (no errors, no memory leaks)
Your file matched the expected output
Testcase passed 
-----------------------------------------------
testcase 7 : Testing with BstMap<(a custom class we wrote),int>
...compiling...
The code compiled.
  - Valgrind was clean (no errors, no memory leaks)
Your file matched the expected output
Testcase passed 
-----------------------------------------------
testcase 8 : Testing with BstMap<(a custom class we wrote),std::string>
...compiling...
The code compiled.
  - Valgrind was clean (no errors, no memory leaks)
Your file matched the expected output
Testcase passed 
-----------------------------------------------
testcase 9 : Testing with BstMap<(a custom class we wrote),(a custom class we wrote)>
...compiling...
The code compiled.
  - Valgrind was clean (no errors, no memory leaks)
Your file matched the expected output
Testcase passed 

Overall Grade: A
