#include <cstdio>
#include <iterator>
#include <vector>

#include "bstmap.h"

class Tester {
  BstMap<int, int> * bstMap;

 public:
  Tester(BstMap<int, int> & mp) : bstMap(&mp){};
  void printTree() {
    printf("Tree: \n");
    bstMap->printNode(bstMap->root);
    printf("\n");
  }
  void addMany(int * arr, int n) {
    for (int i = 0; i < n; i++) {
      bstMap->add(arr[i], arr[i]);
    }
  }
};

int main() {
  BstMap<int, int> mp;
  Tester t(mp);
  int arr[] = {2, 3, 8, -1, -3, 1};
  t.addMany(arr, 6);
  t.printTree();
  BstMap<int, int> mp2;
  mp2 = mp;
  int c = mp2.lookup(1);
  printf("c:%d\n", c);
  int b = mp.lookup(1);
  printf("ok, %d\n", b);
  b = mp.lookup(2);
  printf("ok, %d\n", b);
  try {
    b = mp.lookup(3);
  }
  catch (...) {
    printf("not found\n");
  }
  mp.remove(3);
  t.printTree();
  mp.remove(-1);
  t.printTree();
  mp.remove(1);
  t.printTree();
  mp.remove(10);
  mp.remove(-3);
  mp.remove(8);
  mp.remove(2);
  t.printTree();
  mp.remove(2);
  mp.add(1, 1);
  mp.add(1, 10);
  t.printTree();
}
