#ifndef __LL_HPP__
#define __LL_HPP__

#include <assert.h>

#include <cstdlib>
#include <exception>
#include <stdexcept>

class Tester;

//YOUR CODE GOES HERE
template<typename T>

//typedef int T;

class LinkedList {
 private:
  class Node {
   public:
    T data;
    Node * next;
    Node * prev;
    Node() : data(0), next(NULL), prev(NULL){};
    Node(T d) : data(d), next(NULL), prev(NULL){};
    Node(T d, Node * n, Node * p) : data(d), next(n), prev(p){};
  };
  Node * head;
  Node * tail;
  int size;
  void deleteNodes(Node * head) {
    if (head == NULL) {
      return;
    }
    Node * node = head;
    while (node) {
      Node * tmp = node->next;
      delete node;
      node = tmp;
    }
  };
  Node * copyNodes(Node * head) {
    if (head == NULL) {
      return NULL;
    }
    Node * oldNode = head;
    Node * newHead = new Node(head->data);
    Node * prevNewNode = newHead;
    oldNode = oldNode->next;
    while (oldNode) {
      Node * node = new Node(oldNode->data);
      prevNewNode->next = node;
      node->prev = prevNewNode;
      prevNewNode = node;
      oldNode = oldNode->next;
    }
    return newHead;
  }

 public:
  LinkedList() : head(NULL), tail(NULL), size(0){};
  LinkedList(const LinkedList & rhs) : head(NULL), tail(NULL), size(0) {
    Node * node = rhs.head;
    while (node) {
      addBack(node->data);
      node = node->next;
    }
  };
  LinkedList & operator=(const LinkedList & rhs) {
    if (&rhs != this) {
      Node * tempHead = NULL;
      Node * tempTail = NULL;
      size = rhs.size;
      if (rhs.size > 0) {
        tempHead = copyNodes(rhs.head);
      }
      Node * node = tempHead;
      while (node) {
        tempTail = node;
        node = node->next;
      }
      deleteNodes(head);
      head = tempHead;
      tail = tempTail;
    }
    return *this;
  };
  ~LinkedList() { deleteNodes(head); };
  void addFront(const T & item) {
    Node * node = new Node(item);
    if (size == 0) {
      head = node;
      tail = node;
    }
    else {
      node->next = head;
      head->prev = node;
      head = node;
    }
    size += 1;
  };

  void addBack(const T & item) {
    Node * node = new Node(item);
    if (size == 0) {
      head = node;
      tail = node;
    }
    else {
      tail->next = node;
      node->prev = tail;
      tail = node;
    }
    size += 1;
    //assert(tail->data == item);
  };

  bool remove(const T & item) {
    int idx = find(item);
    if (idx < 0) {
      return false;
    }
    Node * node = head;
    while (idx > 0) {
      node = node->next;
      idx -= 1;
    }

    if (node->prev != NULL) {
      node->prev->next = node->next;
    }
    if (node->next != NULL) {
      node->next->prev = node->prev;
    }
    if (node == head) {
      head = node->next;
    }
    if (node == tail) {
      tail = node->prev;
    }

    delete node;
    size -= 1;
    return true;
  };

  T & operator[](int index) {
    if (index >= size or index < 0) {
      throw std::out_of_range("invalid index");
    }
    Node * node = head;
    while (index > 0) {
      node = node->next;
      index -= 1;
    }
    return node->data;
  };

  const T & operator[](int index) const {
    if (index >= size or index < 0) {
      throw std::out_of_range("invalid index");
    }
    Node * node = head;
    while (index > 0) {
      node = node->next;
      index -= 1;
    }
    return node->data;
  };

  int find(const T & item) {
    Node * node = head;
    int idx = 0;
    while (node) {
      if (node->data == item) {
        return idx;
      }
      idx += 1;
      node = node->next;
    }
    return -1;
  };
  int getSize() const { return size; };
  friend class Tester;
};

#endif
