#include <stdio.h>
#include <stdlib.h>

int max(int a, int b) {
  if (a > b) {
    return a;
  }
  return b;
}

size_t maxSeq(int * array, size_t n) {
  if (n == 0) {
    return 0;
  }
  int cnt = 1;
  int lastNum = array[0];
  int maxLen = 1;
  for (size_t i = 1; i < n; i++) {
    if (array[i] > lastNum) {
      cnt++;
    }
    else {
      maxLen = max(maxLen, cnt);
      cnt = 1;
    }
    lastNum = array[i];
  }
  maxLen = max(cnt, maxLen);
  return maxLen;
}
