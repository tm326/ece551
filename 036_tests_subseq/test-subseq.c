#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

size_t maxSeq(int * array, size_t n);

int main(void) {
  int test1[] = {1, 2, 1, 3, 5, 7, 2, 4, 6, 9, 10};
  assert(maxSeq(test1, 11) == 5);
  assert(maxSeq(test1, 0) == 0);
  int test2[] = {6, 5, 4, 3, 2, 1};
  assert(maxSeq(test2, 6) == 1);
  int test3[] = {-1, -1, -1};
  assert(maxSeq(test3, 3) == 1);
  int test4[] = {-3000, -2000, -1000, -5000};
  assert(maxSeq(test4, 4) == 3);
  int test5[] = {1, 3, 3, 4, 5};
  assert(maxSeq(test5, 5) == 3);
  int test6[] = {-999999, -1000000, 0};
  assert(maxSeq(test6, 3) == 2);
  int test7[] = {-1000000, -1000000, -1000000};
  assert(maxSeq(test7, 3) == 1);
  int test8[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
  assert(maxSeq(test8, 10) == 10);

  return EXIT_SUCCESS;
}
