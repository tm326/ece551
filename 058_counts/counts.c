#include "counts.h"

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
counts_t * createCounts(void) {
  //WRITE ME
  counts_t * counts = malloc(sizeof(*counts));
  assert(counts != NULL);
  counts->length = 0;
  counts->unknown_cnt = 0;
  counts->counts = NULL;
  return counts;
}

one_count_t * createCount(const char * name) {
  one_count_t * count = malloc(sizeof(*count));
  assert(count != NULL);
  count->cnt = 1;
  count->name = name;
  return count;
}

void addCount(counts_t * c, const char * name) {
  //WRITE ME
  // if it is NULL, unknown += 1
  if (name == NULL) {
    c->unknown_cnt++;
    return;
  }

  // if name in counts, add 1
  for (size_t i = 0; i < c->length; i++) {
    if (strcmp(c->counts[i].name, name) == 0) {
      c->counts[i].cnt++;
      //printf("appended: name:%s, count:%d\n", name, (int)c->counts[i].cnt);
      return;
    }
  }
  // if not, create new count
  one_count_t * new_arr = realloc(c->counts, (c->length + 1) * sizeof(*c->counts));
  assert(new_arr != NULL);
  c->counts = new_arr;
  one_count_t * count = createCount(name);
  new_arr[c->length] = *count;
  free(count);
  c->length++;
  //printf("added: name:%s, arrlength:%d\n", name, (int)c->length);
  return;
}
void printCounts(counts_t * c, FILE * outFile) {
  //WRITE ME
  for (size_t i = 0; i < c->length; i++) {
    fprintf(outFile, "%s: %d\n", c->counts[i].name, (int)c->counts[i].cnt);
  }
  if (c->unknown_cnt > 0) {
    fprintf(outFile, "<unknown> : %d\n", (int)c->unknown_cnt);
  }
}

void freeCounts(counts_t * c) {
  //WRITE ME
  free(c->counts);
  free(c);
}
