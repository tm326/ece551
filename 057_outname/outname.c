#include "outname.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char * computeOutputFileName(const char * inputName) {
  //WRITE ME
  char * outname = malloc(strlen(inputName) + 8);
  strcpy(outname, inputName);
  strcpy(outname + strlen(inputName), ".counts");
  return outname;
}
