#include <algorithm>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <vector>

using namespace std;

void printLines(vector<string> lines) {
  for (size_t i = 0; i < lines.size(); i++) {
    cout << lines[i] << endl;
  }
}

void readData(istream & in) {
  string line;
  vector<string> lines;
  while (getline(in, line)) {
    lines.push_back(line);
  }
  sort(lines.begin(), lines.end());
  printLines(lines);
}

int main(int argc, char ** argv) {
  if (argc == 1) {
    readData(cin);
  }
  else {
    for (int i = 1; i < argc; i++) {
      ifstream in;
      in.open(argv[i]);
      if (!in.is_open()) {
        cerr << "fail to open file" << endl;
        exit(EXIT_FAILURE);
      }
      readData(in);
      in.close();
    }
  }
  return EXIT_SUCCESS;
}
