#include "rand_story.h"
#include "stdio.h"

int main(int argc, char ** argv) {
  myassert(argc == 2, "usage: ./program story.txt");
  char * storyTmplt = readStory(argv[1]);
  char * a = fillTemplate(storyTmplt, NULL, 0);
  printf("%s\n", a);
  free(a);
  free(storyTmplt);
  freeWordArr();
}
