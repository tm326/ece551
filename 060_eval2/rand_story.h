#ifndef __RAND_STORY_H__
#define __RAND_STORY_H__

#include "provided.h"

//any functions you want your main to use
void myassert(int condition, const char * errMsg);
char * fillTemplate(const char * tplt, catarray_t * cats, int isNoReuse);
char * readStory(const char * fileName);

catarray_t initCatArr();
void parseWordLine(char * line, catarray_t * cats);
void freeCatArr(catarray_t * cats);
void parseWordFile(const char * fileName, catarray_t * cats);

const char * myChooseWord(char * category, catarray_t * cats, int isNoReuse);
void freeWordArr();
void removeWordInCats(catarray_t * cats, const char * name, const char * word);
void freeRemovedWords();
#endif
