#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "rand_story.h"

int main(int argc, char ** argv) {
  myassert(argc == 3 || argc == 4, "usage: ./program [-n] words.txt story.txt");
  int isNoReuse = 0;
  const char * storyFilename = NULL;
  const char * wordsFilename = NULL;
  if (argc == 3) {
    storyFilename = argv[2];
    wordsFilename = argv[1];
  }
  else {
    myassert(strcmp("-n", argv[1]) == 0, "-n flag doesn't match");
    storyFilename = argv[3];
    wordsFilename = argv[2];
    isNoReuse = 1;
  }
  catarray_t cats = initCatArr();
  parseWordFile(wordsFilename, &cats);
  char * storyTmplt = readStory(storyFilename);
  char * res = fillTemplate(storyTmplt, &cats, isNoReuse);
  printf("%s\n", res);
  free(res);
  freeCatArr(&cats);
  free(storyTmplt);
  freeWordArr();
  freeRemovedWords();
}
