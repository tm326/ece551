#include <stdio.h>
#include <stdlib.h>

#include "rand_story.h"

int main(int argc, char ** argv) {
  myassert(argc == 2, "usage: ./program word.txt");
  catarray_t cats = initCatArr();
  parseWordFile(argv[1], &cats);
  printWords(&cats);
  freeCatArr(&cats);
  return EXIT_SUCCESS;
}
