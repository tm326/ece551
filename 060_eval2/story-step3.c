#include <stdio.h>
#include <stdlib.h>

#include "rand_story.h"

int main(int argc, char ** argv) {
  myassert(argc == 3, "usage: ./program word.txt story.txt");
  catarray_t cats = initCatArr();
  parseWordFile(argv[1], &cats);
  char * storyTmplt = readStory(argv[2]);
  char * res = fillTemplate(storyTmplt, &cats, 0);
  printf("%s\n", res);
  free(res);
  freeCatArr(&cats);
  free(storyTmplt);
  freeWordArr();
}
