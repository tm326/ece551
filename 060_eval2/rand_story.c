#include "rand_story.h"

#include <errno.h>
#include <stdio.h>
#include <string.h>

/*
==========================================================

STEP 1: Fill Template

==========================================================

*/

// my assertion function
void myassert(int condition, const char * errMsg) {
  if (!condition) {
    if (errno != 0) {
      perror(errMsg);
    }
    else {
      fprintf(stderr, "%s\n", errMsg);
    }

    exit(EXIT_FAILURE);
  }
}

// fill the story template with category array
char * fillTemplate(const char * tplt, catarray_t * cats, int isNoReuse) {
  const char * readPos = tplt;
  const char * placeholderStartPos = tplt;
  size_t len = strlen(tplt) + 1;
  char * res = malloc(len * sizeof(*res));
  size_t i = 0;

  // add chars into the new char * res
  // if encounter placeholder, extract the name and replace it with the word in "cats"
  // if word is longer than placeholder, realloc res to fit the word
  while (*readPos != '\0') {
    if (*readPos == '_') {
      placeholderStartPos = readPos;
      readPos++;
      while (*readPos != '_' && *readPos != '\n' && *readPos != '\0') {
        readPos++;
      }
      myassert(*readPos == '_', "placeholder mal-formatted");
      size_t placeholderLen = readPos - placeholderStartPos + 1;  // _placeholder_
      char catQuery[placeholderLen - 1];                          // placeholder
      strncpy(catQuery, placeholderStartPos + 1, placeholderLen - 2);
      catQuery[placeholderLen - 2] = '\0';
      //printf("catQuery:%s\n", catQuery);
      const char * targetWord = myChooseWord(catQuery, cats, isNoReuse);

      // if length of targetWord larger than placeholder, realloc
      if (strlen(targetWord) > placeholderLen) {
        len += strlen(targetWord) - placeholderLen;
        res = realloc(res, len * sizeof(*res));
        myassert(res != NULL, "realloc failed");
      }

      for (size_t j = 0; j < strlen(targetWord); j++, i++) {
        res[i] = targetWord[j];
      }
    }
    else {
      res[i] = *readPos;
      i++;
    }
    readPos++;
  }
  res[i] = '\0';
  return res;
}

// read story template in file
char * readStory(const char * fileName) {
  FILE * f = fopen(fileName, "r");
  myassert(f != NULL, "file open failed");
  char * storyTmplt = NULL;
  size_t sz = 0;
  // read entire file
  // if this is a empty file, just return a empty string.
  if (getdelim(&storyTmplt, &sz, '\0', f) < 0) {
    free(storyTmplt);
    storyTmplt = malloc(sizeof(*storyTmplt));
    storyTmplt[0] = '\0';
  }
  myassert(fclose(f) == 0, "file close failed");
  return storyTmplt;
}

/*
==========================================================

 STEP 2: Fill Category Array

==========================================================


*/

// initialize category array
catarray_t initCatArr() {
  catarray_t cats;
  cats.arr = NULL;
  cats.n = 0;
  return cats;
}

// add a category
category_t * addCat(catarray_t * cats, const char * name) {
  cats->n++;
  cats->arr = realloc(cats->arr, cats->n * sizeof(*cats->arr));
  char * tmpName = malloc((strlen(name) + 1) * sizeof(*tmpName));
  strcpy(tmpName, name);
  cats->arr[cats->n - 1].name = tmpName;
  cats->arr[cats->n - 1].words = NULL;
  cats->arr[cats->n - 1].n_words = 0;
  //printf("category: %s added\n", name);
  return &cats->arr[cats->n - 1];
}

// check whether a category exists in cats, return pointer
category_t * checkCatExist(catarray_t * cats, const char * name) {
  for (size_t i = 0; i < cats->n; i++) {
    if (strcmp(name, cats->arr[i].name) == 0) {
      return &cats->arr[i];
    }
  }
  return NULL;
}

// add a word to category
void addCatWord(catarray_t * cats, const char * name, const char * word) {
  category_t * cat = checkCatExist(cats, name);
  if (cat == NULL) {
    //printf("cat not found, add %s\n", name);
    cat = addCat(cats, name);
  }
  myassert(cat != NULL, "add category failed");

  // realloc words array
  cat->n_words++;
  cat->words = realloc(cat->words, (cat->n_words) * sizeof(*cat->words));
  myassert(cat->words != NULL, "realloc words in cat failed");
  char * tmpWord = malloc((strlen(word) + 1) * sizeof(*tmpWord));
  strcpy(tmpWord, word);
  cat->words[cat->n_words - 1] = tmpWord;
  //printf("%s in %s added", word, name);
}

// free category array
void freeCatArr(catarray_t * cats) {
  for (size_t i = 0; i < cats->n; i++) {
    for (size_t j = 0; j < cats->arr[i].n_words; j++) {
      free(cats->arr[i].words[j]);
    }
    free(cats->arr[i].words);
    free(cats->arr[i].name);
  }
  free(cats->arr);
}

// parse line and add word into category array
void parseWordLine(char * line, catarray_t * cats) {
  const char * name = line;
  char * word = strchr(line, ':');
  myassert(word != NULL, "mal-formatted word");
  *word = '\0';
  word++;
  word[strlen(word) - 1] = '\0';  // word\n\0
  addCatWord(cats, name, word);
}

// read words file and put it into the cat array
void parseWordFile(const char * fileName, catarray_t * cats) {
  FILE * f = fopen(fileName, "r");
  myassert(f != NULL, "file open failed");
  char * line = NULL;
  size_t sz = 0;
  while (getline(&line, &sz, f) >= 0) {
    parseWordLine(line, cats);
    free(line);
    line = NULL;
    sz = 0;
  }
  free(line);
  line = NULL;
  myassert(fclose(f) == 0, "file close failed");
  return;
}

/*
=================================

Step 3: fill template with category array

================================
*/

// use a global variable to store the word reference array
const char ** wordArr = NULL;
size_t wordArrN = 0;

// free word reference array
void freeWordArr() {
  free(wordArr);
}

// encapsule chooseWord to support references to previously used words
// isNoReuse indicating no reuse of words
const char * myChooseWord(char * category, catarray_t * cats, int isNoReuse) {
  char * endPtr = NULL;
  const char * resWord = NULL;
  const long nameLen = strlen(category);
  long refIdx = strtold(category, &endPtr);
  if (endPtr - category != nameLen || nameLen == 0) {  // 123 , len=3
    resWord = chooseWord(category, cats);
    if (isNoReuse) {
      removeWordInCats(cats, category, resWord);
    }
  }
  else if (endPtr - category == nameLen && refIdx > 0 && (size_t)refIdx <= wordArrN) {
    resWord = wordArr[wordArrN - refIdx];
  }
  else {
    myassert(0, "category name or reference index invalid");
  }
  wordArr = realloc(wordArr, (wordArrN + 1) * sizeof(*wordArr));
  wordArrN++;
  wordArr[wordArrN - 1] = resWord;
  return resWord;
}

/*
============================================================

Step 4: No reuse of word

============================================================
 */

// use a global variable to keep track with words that have been removed
char ** removedWords = NULL;
size_t removedWordsN = 0;

// free the removed words array
void freeRemovedWords() {
  for (size_t i = 0; i < removedWordsN; i++) {
    free(removedWords[i]);
  }
  free(removedWords);
}

// remove word in category array
void removeWordInCats(catarray_t * cats, const char * name, const char * word) {
  category_t * cat = checkCatExist(cats, name);  // find the category first
  myassert(cat != NULL, "unable to find the category");
  myassert(cat->n_words > 0, "no words in the category");
  size_t i = 0;
  for (i = 0; i < cat->n_words; i++) {  // find the word
    if (strcmp(word, cat->words[i]) == 0) {
      break;
    }
  }
  myassert(i != cat->n_words, "unable to find the word");
  removedWords = realloc(removedWords, (removedWordsN + 1) * sizeof(*removedWords));
  removedWords[removedWordsN] = cat->words[i];
  removedWordsN++;
  cat->words[i] = cat->words[cat->n_words - 1];
  cat->n_words--;
}
