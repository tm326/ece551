#include "kv.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "assert.h"

kvpair_t * readKVFromLine(char * line) {
  line[strlen(line) - 1] = '\0';
  if (strlen(line) < 1) {
    return NULL;
  }
  char * keyEnd = strchr(line, '=');
  if (keyEnd == NULL) {
    return NULL;
  }
  *keyEnd = '\0';
  kvpair_t * kvPair = malloc(sizeof(*kvPair));
  kvPair->k = line;
  kvPair->v = keyEnd + 1;
  return kvPair;
}

void addKVtoarray(kvarray_t * arr, kvpair_t * kvPair) {
  size_t cnt = arr->count;
  arr->kvs = realloc(arr->kvs, (cnt + 1) * sizeof(*arr->kvs));
  assert(arr->kvs != NULL);
  arr->kvs[cnt] = *kvPair;
  free(kvPair);
  arr->count += 1;
  return;
}

kvarray_t * readKVs(const char * fname) {
  //WRITE ME
  char * line = NULL;
  size_t sz = 0;
  FILE * f = fopen(fname, "r");
  kvarray_t * kvArr = malloc(sizeof(*kvArr));
  kvArr->count = 0;
  kvArr->kvs = NULL;
  while (getline(&line, &sz, f) >= 0) {
    // parse kv into kvstruct
    kvpair_t * kv = readKVFromLine(line);
    if (kv == NULL) {
      break;
    }
    addKVtoarray(kvArr, kv);
    line = NULL;
    sz = 0;
  }
  free(line);
  fclose(f);
  return kvArr;
}

void freeKVs(kvarray_t * pairs) {
  //WRITE ME
  for (size_t i = 0; i < pairs->count; i++) {
    free(pairs->kvs[i].k);
  }
  free(pairs->kvs);
  free(pairs);
}

void printKVs(kvarray_t * pairs) {
  //WRITE ME
  for (size_t i = 0; i < pairs->count; i++) {
    printf("key = '%s' value = '%s'\n", pairs->kvs[i].k, pairs->kvs[i].v);
  }
}

char * lookupValue(kvarray_t * pairs, const char * key) {
  //WRITE ME
  for (size_t i = 0; i < pairs->count; i++) {
    if (strcmp(key, pairs->kvs[i].k) == 0) {
      return pairs->kvs[i].v;
    }
  }
  return NULL;
}
