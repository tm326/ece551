Grading at 11/14/2021:20:43:51.442576
For commit ID ec25c5e5d06a3236510706cee9d36047899a7017
Grading at Sun Nov 14 15:43:46 EST 2021
compiling
g++ -ggdb3 -Wall -Werror -pedantic -o testTree testTree.cpp readFreq.cpp buildTree.cpp
Make succeed, testTree created
Testcase 1
-----------------------
  - Valgrind was clean (no errors, no memory leaks)
Checking output:
Your file matched the expected output
Testcase passed 
Testcase 2
-----------------------
  - Valgrind was clean (no errors, no memory leaks)
Checking output:
Your file matched the expected output
Testcase passed 
Testcase 3
-----------------------
  - Valgrind was clean (no errors, no memory leaks)
Checking output:
Your file matched the expected output
Testcase passed 
Testcase 4
-----------------------
  - Valgrind was clean (no errors, no memory leaks)
Checking output:
Your file matched the expected output
Testcase passed 

Overall Grade: A
