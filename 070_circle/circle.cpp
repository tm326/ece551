#include "circle.hpp"

#include <cmath>
#include <cstdio>

using namespace std;

void Circle::move(double dx, double dy) {
  center.move(dx, dy);
}

double Circle::intersectionArea(const Circle & otherCircle) {
  double d = center.distanceFrom(otherCircle.center);
  double r1 = radius;
  double r2 = otherCircle.radius;
  if (d >= r1 + r2) {
    return 0;
  }
  else if (d <= abs(r1 - r2)) {
    return M_PI * fmin(pow(r1, 2), pow(r2, 2));
  }
  double d1 = (pow(r1, 2) - pow(r2, 2) + pow(d, 2)) / (2 * d);
  double d2 = d - d1;
  return pow(r1, 2) * acos(d1 / r1) + pow(r2, 2) * acos(d2 / r2) -
         d1 * sqrt(pow(r1, 2) - pow(d1, 2)) - d2 * sqrt(pow(r2, 2) - pow(d2, 2));
}
