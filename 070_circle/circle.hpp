#include "point.hpp"

class Circle {
 private:
  Point center;
  double radius;

 public:
  Circle() : center(Point()), radius(0){};
  Circle(Point init_center, double init_radius) :
      center(init_center),
      radius(init_radius){};
  void move(double dx, double dy);
  double intersectionArea(const Circle & otherCircle);
};
