#include <cstdlib>
#include <sstream>
#include <string>

class Expression {
 public:
  virtual std::string toString() const = 0;
  virtual ~Expression() {}
  virtual long evaluate() const = 0;
};

class NumExpression : public Expression {
 private:
  long num;

 public:
  NumExpression(long num_in) : num(num_in) {}
  virtual ~NumExpression() {}
  virtual std::string toString() const {
    std::ostringstream ss;
    ss << num;
    return ss.str();
  }
  virtual long evaluate() const { return num; };
};

enum op_t { OP_PLUS, OP_MINUS, OP_TIMES, OP_DIV };

class TwoOpExpression : public Expression {
 protected:
  Expression * lhs;
  Expression * rhs;
  op_t op;

 public:
  TwoOpExpression(Expression * lhs_in, Expression * rhs_in, op_t o) :
      lhs(lhs_in),
      rhs(rhs_in),
      op(o){};
  virtual ~TwoOpExpression() {
    delete lhs;
    delete rhs;
  };
  virtual std::string toString() const {
    std::string op_s;
    switch (op) {
      case OP_PLUS:
        op_s = "+";
        break;
      case OP_MINUS:
        op_s = "-";
        break;
      case OP_TIMES:
        op_s = "*";
        break;
      case OP_DIV:
        op_s = "/";
        break;
    }
    return "(" + lhs->toString() + " " + op_s + " " + rhs->toString() + ")";
  };
  virtual long evaluate() const = 0;
};

class PlusExpression : public TwoOpExpression {
 public:
  PlusExpression(Expression * lhs_in, Expression * rhs_in) :
      TwoOpExpression(lhs_in, rhs_in, OP_PLUS){};
  virtual long evaluate() const { return lhs->evaluate() + rhs->evaluate(); };
};

class MinusExpression : public TwoOpExpression {
 public:
  MinusExpression(Expression * lhs_in, Expression * rhs_in) :
      TwoOpExpression(lhs_in, rhs_in, OP_MINUS){};
  virtual long evaluate() const { return lhs->evaluate() - rhs->evaluate(); }
};

class TimesExpression : public TwoOpExpression {
 public:
  TimesExpression(Expression * lhs_in, Expression * rhs_in) :
      TwoOpExpression(lhs_in, rhs_in, OP_TIMES){};
  virtual long evaluate() const { return lhs->evaluate() * rhs->evaluate(); }
};
class DivExpression : public TwoOpExpression {
 public:
  DivExpression(Expression * lhs_in, Expression * rhs_in) :
      TwoOpExpression(lhs_in, rhs_in, OP_DIV){};
  virtual long evaluate() const { return lhs->evaluate() / rhs->evaluate(); }
};
