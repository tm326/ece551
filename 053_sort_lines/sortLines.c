#include <assert.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//This function is used to figure out the ordering of the strings
//in qsort. You do not need to modify it.
int stringOrder(const void * vp1, const void * vp2) {
  const char * const * p1 = vp1;
  const char * const * p2 = vp2;
  return strcmp(*p1, *p2);
}
//This function will sort data (whose length is count).
void sortData(char ** data, size_t count) {
  qsort(data, count, sizeof(char *), stringOrder);
}

void printLines(char ** lines, size_t n) {
  for (size_t i = 0; i < n; i++) {
    printf("%s", lines[i]);
  }
}

// Get lines from file, return lines cnt
int getLinesFromFile(char *** lines, FILE * f) {
  assert(*lines == NULL);
  size_t linesCnt = 0;
  size_t linesCap = 8;
  *lines = malloc(linesCap * sizeof(**lines));
  while (1) {
    char * line = NULL;
    size_t sz = 0;
    if (getline(&line, &sz, f) < 0) {
      free(line);
      break;
    }
    if (linesCnt >= linesCap - 1) {
      char ** newLines = realloc(*lines, (linesCap *= 2) * sizeof(*lines));
      assert(newLines != NULL);
      *lines = newLines;
    }
    (*lines)[linesCnt] = line;
    linesCnt++;
  }
  return linesCnt;
}

void freeLines(char ** lines, size_t nLines) {
  for (size_t i = 0; i < nLines; i++) {
    free(lines[i]);
  }
  free(lines);
}

int main(int argc, char ** argv) {
  //WRITE YOUR CODE HERE!
  FILE * f = stdin;
  size_t fileCnt = 1;
  int isStdin = 1;
  if (argc > 1) {
    fileCnt = argc - 1;
    isStdin = 0;
  }
  for (size_t i = 0; i < fileCnt; i++) {
    if (!isStdin) {
      f = fopen(argv[i + 1], "r");
    }
    if (f == NULL) {
      perror("Could not open file");
      return EXIT_FAILURE;
    }
    char ** lines = NULL;
    size_t nLines = getLinesFromFile(&lines, f);
    sortData(lines, nLines);
    printLines(lines, nLines);
    freeLines(lines, nLines);
    if (fclose(f) != 0) {
      perror("Failed to close the input file!");
      return EXIT_FAILURE;
    }
  }
  return EXIT_SUCCESS;
}
