#include "vector.hpp"

#include <cmath>
#include <cstdio>

/* write your class implementation in this file
 */

using namespace std;

double Vector2D::getMagnitude() const {
  return sqrt(pow(x, 2) + pow(y, 2));
}

Vector2D Vector2D::operator+(const Vector2D & rhs) const {
  Vector2D res;
  res.initVector(rhs.x + x, rhs.y + y);
  return res;
}

Vector2D & Vector2D::operator+=(const Vector2D & rhs) {
  x += rhs.x;
  y += rhs.y;
  return *this;
}

double Vector2D::dot(const Vector2D & rhs) const {
  return rhs.x * x + rhs.y * y;
}

void Vector2D::print() const {
  printf("<%.2f, %.2f>", x, y);
}
