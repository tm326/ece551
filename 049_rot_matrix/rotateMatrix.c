#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>

void assert(int expr, char * info) {
  if (expr) {
    return;
  }
  fprintf(stderr, "%s\n", info);
  exit(EXIT_FAILURE);
}

void getMatrix(FILE * f, char matrix[][10]) {
  char line[12];
  int n = 10;
  for (size_t i = 0; i < n; i++) {
    assert(fgets(line, 12, f) != NULL, "not enough lines");
    assert(line[10] == '\n', "column does not match");
    for (size_t j = 0; j < n; j++) {
      assert(line[i] != '\n' && line[i] != '\0', "not enough column");
      matrix[i][j] = line[j];
    }
  }
  assert(fgets(line, 12, f) == NULL, "too many lines");
}

void rotateMatrix(char src[][10], char dst[][10]) {
  int n = 10;
  for (size_t i = 0; i < n; i++) {
    for (size_t j = 0; j < n; j++) {
      dst[i][j] = src[n - 1 - j][i];
    }
  }
}

void printMatrix(char matrix[][10]) {
  size_t n = 10;
  for (size_t i = 0; i < n; i++) {
    for (size_t j = 0; j < n; j++) {
      printf("%c", matrix[i][j]);
    }
    printf("\n");
  }
}

int main(int argc, char ** argv) {
  if (argc != 2) {
    fprintf(stderr, "filename needed");
    return EXIT_FAILURE;
  }
  FILE * f = fopen(argv[1], "r");
  if (f == NULL) {
    perror("file not found");
    return EXIT_FAILURE;
  }
  char matrix[10][10];
  char rotated[10][10];
  getMatrix(f, matrix);
  //printMatrix(matrix);
  if (fclose(f) != 0) {
    perror("file close error");
    return EXIT_FAILURE;
  }
  rotateMatrix(matrix, rotated);
  printMatrix(rotated);
  return EXIT_SUCCESS;
}
