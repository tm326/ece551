#include "pandemic.h"

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>

#include "string.h"

// customized assert function, print error info to stderr
void myAssert(int expr, char * info) {
  if (expr) {
    return;
  }
  if (errno != 0) {
    perror("failed");
  }
  fprintf(stderr, "%s\n", info);
  exit(EXIT_FAILURE);
}

// parse country info from csv to country_t
country_t parseLine(char * line) {
  //WRITE ME
  myAssert(line != NULL, "line not exists");
  country_t ans;
  char * str = line;
  ans.name[0] = '\0';
  size_t i = 0;

  while (*str != ',' && *str != '\0' && *str != '\n') {
    // whether the len of the country name under limit
    myAssert(i < MAX_NAME_LEN - 1, "country_name_len over the MAX_NAME_LEN");
    ans.name[i] = *str;
    i++;
    str++;
  }
  // at the end of the country name, it must be a comma
  myAssert(*str == ',', "mal-formatted input");
  ans.name[i] = '\0';
  str++;

  char * endPointer = str;
  ans.population = strtoul(str, &endPointer, 10);
  myAssert(errno == 0, "convert failed");
  myAssert(endPointer != str, "convert failed, mal-formatted data");
  myAssert(ans.population >= 0, "population cannot be negative");
  // ignore the following chars
  return ans;
}

// calc running avg
void calcRunningAvg(unsigned * data, size_t n_days, double * avg) {
  //WRITE ME
  myAssert(avg != NULL, "avg not exist");
  unsigned runningSum = 0;
  for (size_t i = 0; i < n_days; i++) {
    if (i > 6) {
      runningSum -= data[i - 7];
    }
    runningSum += data[i];
    if (i > 5) {
      avg[i - 6] = (double)runningSum / 7;
    }
  }
  return;
}

// calc cumulative cases
void calcCumulative(unsigned * data, size_t n_days, uint64_t pop, double * cum) {
  //WRITE ME
  myAssert(cum != NULL, "cum not exist");
  unsigned cumulate = 0;
  for (size_t i = 0; i < n_days; i++) {
    cumulate += data[i];
    cum[i] = (double)cumulate / pop * 100000;
  }
  return;
}

// find the maximum daily cases
void printCountryWithMax(country_t * countries,
                         size_t n_countries,
                         unsigned ** data,
                         size_t n_days) {
  //WRITE ME
  size_t maxIdxCountry = 0;

  unsigned maxCase = 0;
  unsigned maxTimes = 0;
  int isSameCountryFlag = 0;

  // use a nested loop to find the maximum value and its indexs
  for (size_t day = 0; day < n_days; day++) {
    for (size_t countryIdx = 0; countryIdx < n_countries; countryIdx++) {
      unsigned val = 0;
      val = data[countryIdx][day];
      if (val == maxCase) {
        maxTimes++;
        isSameCountryFlag &= maxIdxCountry == countryIdx;
      }
      if (val > maxCase) {
        maxTimes = 1;
        maxIdxCountry = countryIdx;
        isSameCountryFlag = 1;
        maxCase = val;
      }
    }
  }
  if (maxTimes > 1 && !isSameCountryFlag) {
    printf("There is a tie between at least two countries\n");
  }
  else {
    printf(
        "%s has the most daily cases with %u\n", countries[maxIdxCountry].name, maxCase);
  }

  return;
}
