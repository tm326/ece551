#error
printCountries population_wrong.csv
printRunningAvg population_wrong.csv daily_tiny.csv
printCumulative population_wrong.csv daily_small.csv
printDailyMax population_wrong.csv daily_small.csv
printDailyMax population_wrong.csv daily_empty.csv

#success
printCountries population.csv
printRunningAvg population.csv daily_tiny.csv
printRunningAvg population.csv daily_small.csv
printRunningAvg population.csv daily_cases.csv
printDailyMax population.csv daily_small.csv
printDailyMax population.csv daily_tiny.csv
printDailyMax population.csv daily_cases.csv
printCumulative population.csv daily_small.csv
printCumulative population.csv daily_tiny.csv
printCumulative population.csv daily_empty.csv
printCumulative population.csv daily_cases.csv
printCumulative population_zero.csv daily_cases.csv
printRunningAvg population_zero.csv daily_small.csv
printDailyMax population_zero.csv daily_cases.csv
printCountries population_zero.csv
printCumulative population_zero.csv daily_small.csv
printDailyMax population.csv daily_country_ties.csv
