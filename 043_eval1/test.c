#include <stdio.h>
#include <stdlib.h>

#include "./pandemic.h"

int main(void) {
  unsigned data[] = {27, 0, 0, 17, 0, 15, 0, 0, 0, 0};
  size_t n_days = 10;
  double avg[10] = {0};
  printf("hello\n");
  calcRunningAvg(data, n_days, avg);
  for (int i = 0; i < 10; i++) {
    printf("%f,", avg[i]);
  }
  printf("\n");
}
