#include <assert.h>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>

int arrMaxIndex(int * arr, int n) {
  assert(n > 0);
  int maxVal = arr[0];
  int maxIndex = 0;
  for (int i = 0; i < n; i++) {
    if (arr[i] > maxVal) {
      maxIndex = i;
      maxVal = arr[i];
    }
  }
  //printf("input:%s, size:%d, maxIndex:%d", arr, )
  return maxIndex;
}

int getKey(FILE * f) {
  if (f == NULL) {
    return 0;
  }
  int cnts[26] = {0};
  char c;
  int cnt = 0;
  while ((c = fgetc(f)) != EOF) {
    if (isalpha(c)) {
      cnts[c - 'a']++;
      cnt++;
    }
  }
  if (cnt == 0) {
    return -99;
  }
  int idx = arrMaxIndex(cnts, 26);
  //printf("idx:%d\n", idx);
  int res = (idx - (int)('e' - 'a')) % 26;
  return res >= 0 ? res : res + 26;
}

int main(int argc, char ** argv) {
  if (argc != 2) {
    fprintf(stderr, "filename needed");
    return EXIT_FAILURE;
  }
  FILE * f = fopen(argv[1], "r");
  if (f == NULL) {
    perror("file not found");
    return EXIT_FAILURE;
  }
  int key = getKey(f);
  if (fclose(f) != 0) {
    perror("file close error");
    return EXIT_FAILURE;
  }
  if (key == -99) {
    fprintf(stderr, "no sufficient letters");
    return EXIT_FAILURE;
  }
  printf("%d\n", key);
  return EXIT_SUCCESS;
}
