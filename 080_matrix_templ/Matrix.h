#ifndef __T_MATRIX_H___
#define __T_MATRIX_H___

#include <assert.h>

#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <vector>

using namespace std;

//YOUR CODE GOES HERE!
template<typename T>
class Matrix {
 private:
  int numRows;
  int numColumns;
  std::vector<std::vector<T> > data;

 public:
  Matrix() : numRows(0), numColumns(0), data(0){};
  Matrix(int r, int c) : numRows(r), numColumns(c), data(r, vector<T>(c)){};
  Matrix(const Matrix & rhs) :
      numRows(rhs.numRows),
      numColumns(rhs.numColumns),
      data(rhs.data){};
  Matrix & operator=(const Matrix & rhs) {
    if (this != &rhs) {
      numRows = rhs.numRows;
      numColumns = rhs.numColumns;
      data = rhs.data;
    }
    return *this;
  };
  int getRows() const { return numRows; };
  int getColumns() const { return numColumns; };
  const std::vector<T> & operator[](int index) const {
    assert(index >= 0 && index < numRows);
    return data[index];
  };
  std::vector<T> & operator[](int index) {
    assert(index >= 0 && index < numRows);
    return data[index];
  };
  bool operator==(const Matrix & rhs) const {
    if (numRows != rhs.numRows) {
      return false;
    }
    return data == rhs.data;
  };
  Matrix operator+(const Matrix & rhs) const {
    assert(numRows == rhs.numRows && numColumns == rhs.numColumns);
    Matrix<T> newMatrix(*this);
    for (int i = 0; i < numRows; i++) {
      for (int j = 0; j < numColumns; j++) {
        newMatrix[i][j] += rhs[i][j];
      }
    }
    return newMatrix;
  };
};

template<typename T>
std::ostream & operator<<(std::ostream & s, const Matrix<T> & rhs) {
  s << "[";

  for (int i = 0; i < rhs.getRows(); i++) {
    if (i > 0) {
      s << ", " << endl;
    }
    s << rhs[i];
  }
  s << "]" << endl;
  return s;
}

template<typename T>
std::ostream & operator<<(std::ostream & s, const std::vector<T> & rhs) {
  s << "{";
  for (size_t i = 0; i < rhs.size(); i++) {
    if (i > 0) {
      s << ", ";
    }
    s << rhs[i];
  }
  s << "}";
  return s;
}

#endif
