#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

unsigned power(unsigned x, unsigned y);

int main(void) {
  assert(power(2, 2) == 4);
  assert(power(5, 0) == 1);
  assert(power(-1, 2) == 1);
  assert(power(0, 0) == 1);
  assert(power(0, 1) == 0);
  assert(power(65535, 2) == 65535 ^ 2);
  assert(power(-1, 3) == -1);
  return EXIT_SUCCESS;
}
