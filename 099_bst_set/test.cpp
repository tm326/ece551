#include <cassert>
#include <cstdio>
#include <iterator>
#include <vector>

#include "bstset.h"

int main() {
  BstSet<int> set;
  set.add(10);
  set.add(10);
  set.add(100);
  set.add(1);
  assert(set.contains(1));
  assert(!set.contains(2));
  assert(set.contains(10));
  assert(set.contains(100));
  set.remove(100);
  assert(!set.contains(100));
}
