#include <iostream>
#include <stdexcept>
#include <vector>

#include "set.h"
class Tester;

template<typename T>
//typedef int T;
class BstSet : public Set<T> {
 private:
  class Node {
   public:
    T key;
    Node * left;
    Node * right;
    Node() : key(0), left(NULL), right(NULL){};
    Node(T k) : key(k), left(NULL), right(NULL){};
    Node(Node const & rhs) : key(rhs.key), left(NULL), right(NULL){};
  };
  Node * root;
  void removeNodes(Node * node) {
    if (node == NULL) {
      return;
    }
    removeNodes(node->left);
    removeNodes(node->right);
    delete node;
    return;
  }
  Node * removeNode(Node * node, T key) {
    if (node == NULL) {
      return NULL;
    }
    if (key == node->key) {
      if (node->left == NULL && node->right == NULL) {
        delete node;
        return NULL;
      }
      else if (node->left == NULL) {
        Node * tmp = node->right;
        delete node;
        return tmp;
      }
      else if (node->right == NULL) {
        Node * tmp = node->left;
        delete node;
        return tmp;
      }
    }

    node->left = removeNode(node->left, key);
    node->right = removeNode(node->right, key);
    return node;
  }
  Node * findNode(const T & key) {
    Node ** nodePtr = &root;
    while (*nodePtr != NULL) {
      if ((*nodePtr)->key == key) {
        return *nodePtr;
      }
      else if (key > (*nodePtr)->key) {
        nodePtr = &((*nodePtr)->right);
      }
      else {
        nodePtr = &((*nodePtr)->left);
      }
    }
    return NULL;
  }
  void printNode(Node * node) const {
    if (node == NULL) {
      return;
    }
    std::cout << node->key << " ";
    printNode(node->left);
    printNode(node->right);
  }
  Node * copyNodes(Node * node) const {
    if (node == NULL) {
      return NULL;
    }
    Node * newNode = new Node(*node);
    newNode->left = copyNodes(node->left);
    newNode->right = copyNodes(node->right);
    return newNode;
  }

 public:
  BstSet() : root(NULL){};
  BstSet(const BstSet & rhs) : root(NULL) { root = copyNodes(rhs.root); };
  BstSet & operator=(const BstSet & rhs) {
    if (&rhs != this) {
      Node * newRoot = copyNodes(rhs.root);
      removeNodes(root);
      root = newRoot;
    }
    return *this;
  }
  virtual void add(const T & key) {
    Node ** nodePtr = &root;
    while (true) {
      if (*nodePtr == NULL) {
        *nodePtr = new Node(key);
        return;
      }
      else if ((*nodePtr)->key == key) {
        return;
      }
      else if (key > (*nodePtr)->key) {
        nodePtr = &((*nodePtr)->right);
      }
      else {
        nodePtr = &((*nodePtr)->left);
      }
    }
    return;
  };
  virtual bool contains(const T & key) const {
    Node * const * nodePtr = &root;
    while (*nodePtr != NULL) {
      if ((*nodePtr)->key == key) {
        return true;
      }
      else if (key > (*nodePtr)->key) {
        nodePtr = &((*nodePtr)->right);
      }
      else {
        nodePtr = &((*nodePtr)->left);
      }
    }
    return false;
  };
  virtual void remove(const T & key) {
    Node * node = findNode(key);
    if (node == NULL) {
      return;
    }
    // 1 swap if both have left and right child
    if (node->left && node->right) {
      // find largest key smaller than current and swap
      Node * swapNode = node->left;
      while (swapNode->right) {
        swapNode = swapNode->right;
      }
      T tmpK = node->key;
      node->key = swapNode->key;
      swapNode->key = tmpK;
      node = swapNode;
    }
    // 2 delete Node
    root = removeNode(root, key);
  };
  virtual ~BstSet() {
    removeNodes(root);
    return;
  };
  friend class Tester;
};
