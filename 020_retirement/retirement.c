#include <stdio.h>
#include <stdlib.h>

struct _retire_info {
  int months;
  double contribution;
  double rate_of_return;
};

typedef struct _retire_info retire_info;

double calcBalance(double originalBalance, double debit, double rate) {
  return originalBalance * (1 + rate / 12) + debit;
}

void retirement(int startAge, double initial, retire_info working, retire_info retired) {
  int currentAge = startAge;
  double currentBalance = initial;
  for (int i = 0; i < working.months; i++) {
    printf("Age %3d month %2d you have $%.2lf\n",
           currentAge / 12,
           currentAge % 12,
           currentBalance);
    currentBalance =
        calcBalance(currentBalance, working.contribution, working.rate_of_return);
    currentAge++;
  }
  for (int i = 0; i < retired.months; i++) {
    printf("Age %3d month %2d you have $%.2lf\n",
           currentAge / 12,
           currentAge % 12,
           currentBalance);
    currentBalance =
        calcBalance(currentBalance, retired.contribution, retired.rate_of_return);
    currentAge++;
  }
}

int main(void) {
  retire_info working, retired;

  working.months = 489;
  working.contribution = 1000;
  working.rate_of_return = 0.045;

  retired.months = 384;
  retired.contribution = -4000;
  retired.rate_of_return = 0.01;

  retirement(327, 21345, working, retired);

  return EXIT_SUCCESS;
}
